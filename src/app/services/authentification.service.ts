import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../classes/user';
import { DonneePersonnelles } from '../classes/donnee-personnelles';

@Injectable({
    providedIn: 'root'
})
export class AuthentificationService {

    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(email: string, password: string) {
        // return this.http.post<any>(`http://localhost:8082/utlisateur/login`, { email, password })
        //     .pipe(map(user => {
        //         console.log(user);
        //         localStorage.setItem('currentUser', JSON.stringify(user));
        //         this.currentUserSubject.next(user);
        //         return user;
        //     }));
        const user = {
            id: 1,
            username: "admin",
            email: "admin",
            password: "admin",
            firstName: "admin",
            lastName: "admin",
            isBlocked: false,
            isAdmin: true,
            isVendeur: true
        }
        if (user.email == email && user.password == password) {
            localStorage.setItem('currentUser', JSON.stringify(user));
            return user;
        } else {
            return null;
        }
    }

    donneesPersonnelles(id: number) {
        return this.http.get<User>(`localhost/user/donneePersonnelles/` + id)
            .pipe(map(user => {
                return user;
            }));
    }

    updateDonneesPersonnelles(data: DonneePersonnelles, id: number) {
        return this.http.post<User>(`localhost/user/donneePersonnellesUpdate/` + id, { data })
            .pipe(map(user => {
                this.currentUserSubject.next(user);
                return user;
            }));
    }


    adressePersonnelles(id: number) {
        return this.http.get<User>(`localhost/user/adressePersonnelle/` + id)
            .pipe(map(user => {
                return user;
            }));
    }

    updateAdressePersonnelles(data: DonneePersonnelles, id: number) {
        return this.http.post<User>(`localhost/user/adressePersonnelleUpdate/` + id, { data })
            .pipe(map(user => {
                this.currentUserSubject.next(user);
                return user;
            }));
    }

    updateisVendeur(data: boolean) {
        return this.http.post<User>(`localhost/user/isVendeur`, { data })
            .pipe(map(user => {
                this.currentUserSubject.next(user);
                return user;
            }));
    }

    updateisBlocked(data: boolean) {
        return this.http.post<User>(`localhost/user/isBlocked `, { data })
            .pipe(map(user => {
                this.currentUserSubject.next(user);
                return user;
            }));
    }

    logout() {
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}
