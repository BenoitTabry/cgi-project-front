import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class HomeService {
  private _Url = "http://localhost:8081/artistes/findAll";

  constructor(private _http: HttpClient) {}
  getartiste(): Observable<artistes[]> {
    return this._http.get<artistes[]>(this._Url);
  }
  GetartisteId(i: number): Observable<artistes> {
    return this.getartiste().pipe(
      map(txs => txs.find(txn => txn.id_artist === i))
    );
  }
}
