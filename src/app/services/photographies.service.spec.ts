import { TestBed } from '@angular/core/testing';

import { PhotographiesService } from './photographies.service';

describe('PhotographiesService', () => {
  let service: PhotographiesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PhotographiesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
