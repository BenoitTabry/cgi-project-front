import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Photographies } from '../classes/photographies';

@Injectable({
  providedIn: "root"
})
export class PhotographiesService {
  private _Url = "http://localhost:8082/photographies/photographies";

  constructor(private _http: HttpClient) { }

  getAllPhotographies(): Observable<Photographies[]> {
    return this._http.get<Photographies[]>(this._Url);
  }

  getBestPhotographies(): Observable<Photographies[]> {
    return this._http.get<Photographies[]>(this._Url + "/best");
  }

  getNewsPhotographies(): Observable<Photographies[]> {
    return this._http.get<Photographies[]>(this._Url + "/news");
  }

  getPhotographiesById(): Observable<Photographies> {
    return this._http.get<Photographies>(this._Url + "/2");
  }
}