import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

import { User } from '../classes/user';

@Injectable({
  providedIn: "root"
})
export class UtilisateurService {
  private _Url = "http://localhost:8080/";

  constructor(private _http: HttpClient) {}

  getAllUsers(): Observable<User[]> {
    return this._http.get<User[]>(this._Url + "users");
  }

  getUserById(id: number): Observable<User> {
    return this._http.get<User>(this._Url + "user/" + id);
  }
}
