import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class CommandesService {
  private _Url = " http://localhost:8081/commandes/findAll";

  constructor(private _http: HttpClient) {}
  getcommandes(): Observable<commandes[]> {
    return this._http.get<commandes[]>(this._Url);
  }
  GetcommandeId(i: number): Observable<commandes> {
    return this.getcommandes().pipe(map(txs => txs.find(txn => txn.id === i)));
  }
}
