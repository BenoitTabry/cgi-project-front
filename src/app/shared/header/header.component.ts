import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthentificationService } from '../../services/authentification.service';
import { User } from '../../classes/user';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  currentUser: User;
  panier: string;

  constructor(private router: Router, private authenticationService: AuthentificationService, private storage: StorageService) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit(): void {
    this.storage.watchStorage().subscribe(data => {
      this.panier = data;
    });
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/']);
  }
}