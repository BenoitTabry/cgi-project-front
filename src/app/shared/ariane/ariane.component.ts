import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-ariane',
  templateUrl: './ariane.component.html',
  styleUrls: ['./ariane.component.scss']
})
export class ArianeComponent implements OnInit {

  @Input() ariane1: string;
  @Input() ariane2: string;
  @Input() link1: string;
  @Input() link2: string;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  onLink1() {
    this.router.navigate([this.link1]);
  }

  onLink2() {
    this.router.navigate([this.link2]);
  }

}
