import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-gestion-commandes',
  templateUrl: './gestion-commandes.component.html',
  styleUrls: ['./gestion-commandes.component.scss']
})
export class GestionCommandesComponent implements OnInit {

  constructor(private _location: Location) { }

  ngOnInit(): void {
  }

  submit() {
    this._location.back();
  }

}
