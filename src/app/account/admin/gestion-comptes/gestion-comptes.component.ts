import { Component, OnInit } from '@angular/core';
import { AuthentificationService } from 'src/app/services/authentification.service';
import { User } from 'src/app/classes/user';
import { Location } from '@angular/common';

@Component({
  selector: 'app-gestion-comptes',
  templateUrl: './gestion-comptes.component.html',
  styleUrls: ['./gestion-comptes.component.scss']
})
export class GestionComptesComponent implements OnInit {
  currentUser: User;

  constructor(private authenticationService: AuthentificationService, private _location: Location) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }


  ngOnInit(): void {
  }

  submit() {
    this._location.back();
  }

  setVendeur() {
    this.authenticationService.updateisVendeur(!this.currentUser.isVendeur);
  }

  setBloquer() {
    this.authenticationService.updateisBlocked(!this.currentUser.isBlocked);
  }
}
