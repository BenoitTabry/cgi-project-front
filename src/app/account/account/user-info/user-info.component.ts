import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthentificationService } from '../../../services/authentification.service';
import { User } from '../../../classes/user';
import { first } from 'rxjs/operators';
import { Location } from '@angular/common';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {
  currentUser: User;
  userInfo: any;
  userForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private router: Router, private authenticationService: AuthentificationService, private _location: Location) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      civilite: [''],
      prenom: [''],
      nom: [''],
      telephone: [''],
    });
    // this.userInfo = this.authenticationService.donneesPersonnelles(this.currentUser.id);
  }

  get form() {
    return this.userForm.controls;
  }


  onSubmit() {
    //   if (this.userForm.invalid) {
    //     return;
    //   }

    //   this.authenticationService.updateDonneesPersonnelles(this.userForm.value, this.currentUser.id)
    //     .pipe(first())
    //     .subscribe(
    //       data => {
    //         this.router.navigate(["/account"]);
    //       });
    // }
    this._location.back();
  }
}
