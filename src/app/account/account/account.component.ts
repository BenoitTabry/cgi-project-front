import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthentificationService } from '../../services/authentification.service';
import { User } from '../../classes/user';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  currentUser: User;
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  user: User;

  constructor(private formBuilder: FormBuilder, private router: Router, private authenticationService: AuthentificationService) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;
    this.user = this.authenticationService.login(this.f.email.value, this.f.password.value);
    if (this.user == null) {
      this.error = "Login ou Mot de passe incorrecte";
    }
    if (this.user.isBlocked == false) {
      location.reload();
    }
    else
      this.error = "Compte bloqué, veuillez contacter le support.";
  }

}
