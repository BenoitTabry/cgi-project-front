import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthentificationService } from '../../../services/authentification.service';
import { User } from '../../../classes/user';
import { first } from 'rxjs/operators';
import { Location } from '@angular/common';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit {
  currentUser: User;
  userInfo: any;
  userForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private router: Router, private authenticationService: AuthentificationService, private _location: Location) { 
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      rue: [''],
      cp: [''],
      ville: [''],
      pays: [''],
    });
    // this.userInfo = this.authenticationService.adressePersonnelles(this.currentUser.id);
  }
  
  get form() {
    return this.userForm.controls;
  }

  
  onSubmit() {
    // if (this.userForm.invalid) {
    //   return;
    // }
    // this.authenticationService.updateAdressePersonnelles(this.userForm.value, this.currentUser.id)
    //   .pipe(first())
    //   .subscribe(
    //     data => {
    //       this.router.navigate(["/account"]);
    //     });
    
    this._location.back();
  }

}
