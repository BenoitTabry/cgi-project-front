import { Component, OnInit } from '@angular/core';
import { PhotographiesService } from 'src/app/services/photographies.service';
import { Photographies } from 'src/app/classes/photographies';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  bestPhotographies: Photographies[];

  constructor(private ps: PhotographiesService) { }

  ngOnInit(): void {
    this.ps.getBestPhotographies().subscribe(data => {
      this.bestPhotographies = data.slice(0, 6);
    })
  }

}
