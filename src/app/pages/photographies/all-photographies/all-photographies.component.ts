import { Component, OnInit } from '@angular/core';
import { Photographies } from 'src/app/classes/photographies';
import { PhotographiesService } from 'src/app/services/photographies.service';

@Component({
  selector: 'app-all-photographies',
  templateUrl: './all-photographies.component.html',
  styleUrls: ['./all-photographies.component.scss']
})
export class AllPhotographiesComponent implements OnInit {
  p: any;
  photos: Photographies[];

  constructor(private ps: PhotographiesService) { }

  ngOnInit(): void {
    this.ps.getAllPhotographies().subscribe(data => {
      this.photos = data;
    });
  }

}
