import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllPhotographiesComponent } from './all-photographies.component';

describe('AllPhotographiesComponent', () => {
  let component: AllPhotographiesComponent;
  let fixture: ComponentFixture<AllPhotographiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllPhotographiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllPhotographiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
