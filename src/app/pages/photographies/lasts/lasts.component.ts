import { Component, OnInit } from '@angular/core';
import { Photographies } from 'src/app/classes/photographies';
import { PhotographiesService } from 'src/app/services/photographies.service';

@Component({
  selector: 'app-lasts',
  templateUrl: './lasts.component.html',
  styleUrls: ['./lasts.component.scss']
})
export class LastsComponent implements OnInit {
  p: any;
  photos: Photographies[];

  constructor(private ps: PhotographiesService) { }

  ngOnInit(): void {
    this.ps.getBestPhotographies().subscribe(data => {
      this.photos = data;
    });
  }

}
