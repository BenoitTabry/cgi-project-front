import { Component, OnInit } from '@angular/core';
import { PhotographiesService } from 'src/app/services/photographies.service';
import { Photographies } from 'src/app/classes/photographies';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  p: any;
  photos: Photographies[];

  constructor(private ps: PhotographiesService) { }

  ngOnInit(): void {
    this.ps.getNewsPhotographies().subscribe(data => {
      this.photos = data;
    });
  }

}
