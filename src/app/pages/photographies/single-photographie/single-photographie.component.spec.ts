import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SinglePhotographieComponent } from './single-photographie.component';

describe('SinglePhotographieComponent', () => {
  let component: SinglePhotographieComponent;
  let fixture: ComponentFixture<SinglePhotographieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SinglePhotographieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SinglePhotographieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
