import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';
import { Photographies } from 'src/app/classes/photographies';
import { PhotographiesService } from 'src/app/services/photographies.service';

@Component({
  selector: 'app-single-photographie',
  templateUrl: './single-photographie.component.html',
  styleUrls: ['./single-photographie.component.scss']
})
export class SinglePhotographieComponent implements OnInit {
  sub: any;
  ariane1: string;
  link1: string;
  monObjet: string;
  photo: Photographies;

  constructor(private route: ActivatedRoute, private router: Router, private storage: StorageService, private ps: PhotographiesService) { }

  ngOnInit(): void {
    this.ps.getPhotographiesById().subscribe(item => {
      this.photo = item;
    });
    this.sub = this.route.queryParams
      .subscribe(params => {
        this.ariane1 = params['ariane1'];
        this.link1 = params['link1'];
      });
  }

  addPanier() {
    this.storage.setItem('panier', "1");
    this.router.navigate(['/home']);
  }
}
