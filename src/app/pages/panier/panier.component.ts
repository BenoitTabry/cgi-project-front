import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { Photographies } from 'src/app/classes/photographies';
import { PhotographiesService } from 'src/app/services/photographies.service';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.scss']
})
export class PanierComponent implements OnInit {

  panier: string;
  photo: Photographies;
  
  constructor(private storage: StorageService, private ps: PhotographiesService) {
   }

  ngOnInit(): void {
    this.panier = localStorage.getItem("panier") == '1' ? "added" : "removed";
    this.ps.getPhotographiesById().subscribe(data => {
      this.photo = data;
    })

  }

  removePanier() {
    this.storage.removeItem("panier");
    this.panier = "removed";
  }

}
