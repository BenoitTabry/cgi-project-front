import { Component, OnInit } from '@angular/core';
import '../../../assets/smtp.js';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
declare let Email: any;

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit {
  helpForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.helpForm = this.formBuilder.group({
      civilite: ['', Validators.required],
      name: ['', Validators.required],
      surname: ['', Validators.required],
      email: ['', Validators.required],
      telephone: [''],
      subject: ['', Validators.required],
      message: ['', Validators.required],
    });
  }

  get form() {
    return this.helpForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.helpForm.invalid) {
      return;
    }
    Email.send({
      Host: "smtp.elasticemail.com",
      Username: "kartina.help@gmail.com",
      Password: "0096761834E6C06D200376FDCAEFA21FA122",
      To: "kartina.help@gmail.com",
      From: "kartina.help@gmail.com",
      Subject: this.form.subject.value,
      Body: this.form.message.value + "<br /> From: " + this.form.civilite.value + " " + this.form.name.value + " " + this.form.surname.value
      + "<br />" + this.form.email.value + " " + this.form.telephone.value
    }).then(message => {
      alert("Email Envoyé");
      this.router.navigate(['/'])
    });

  }
}