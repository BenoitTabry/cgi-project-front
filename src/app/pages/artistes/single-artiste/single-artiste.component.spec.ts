import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleArtisteComponent } from './single-artiste.component';

describe('SingleArtisteComponent', () => {
  let component: SingleArtisteComponent;
  let fixture: ComponentFixture<SingleArtisteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleArtisteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleArtisteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
