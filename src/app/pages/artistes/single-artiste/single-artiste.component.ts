import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PhotographiesService } from 'src/app/services/photographies.service';
import { Photographies } from 'src/app/classes/photographies';

@Component({
  selector: 'app-single-artiste',
  templateUrl: './single-artiste.component.html',
  styleUrls: ['./single-artiste.component.scss']
})
export class SingleArtisteComponent implements OnInit {
  sub: any;
  ariane1: string;
  link1: string;
  photos: Photographies[];

  constructor(private route: ActivatedRoute, private ps: PhotographiesService) { }

  ngOnInit(): void {
    this.sub = this.route.queryParams
      .subscribe(params => {
        this.ariane1 = params['ariane'];
        this.link1 = params['link'];
      });

    this.ps.getAllPhotographies().subscribe(data => {
      this.photos = data.slice(0, 4);
    })
  }
}
