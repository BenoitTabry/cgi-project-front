import { Route } from '@angular/router';
import { HomeComponent } from '../pages/home/home.component';
import { AllPhotographiesComponent } from '../pages/photographies/all-photographies/all-photographies.component';
import { NewsComponent } from '../pages/photographies/news/news.component';
import { ArtistesComponent } from '../pages/artistes/artistes/artistes.component';
import { SingleArtisteComponent } from '../pages/artistes/single-artiste/single-artiste.component';
import { PanierComponent } from '../pages/panier/panier.component';
import { AccountComponent } from '../account/account/account.component';
import { CreateAccountComponent } from '../account/create-account/create-account.component';
import { HelpComponent } from '../pages/help/help.component';
import { MentionLegalesComponent } from '../pages/footer-pages/mention-legales/mention-legales.component';
import { ConditionUtilisationComponent } from '../pages/footer-pages/condition-utilisation/condition-utilisation.component';
import { NotFoundComponent } from '../pages/not-found/not-found.component';
import { LastsComponent } from '../pages/photographies/lasts/lasts.component';
import { SinglePhotographieComponent } from '../pages/photographies/single-photographie/single-photographie.component';
import { AddressComponent } from '../account/account/address/address.component';
import { UserInfoComponent } from '../account/account/user-info/user-info.component';
import { OrdersComponent } from '../account/account/orders/orders.component';
import { GestionComptesComponent } from '../account/admin/gestion-comptes/gestion-comptes.component';
import { GestionCommandesComponent } from '../account/admin/gestion-commandes/gestion-commandes.component';

export const appRoutes: Route[] = [
    { path: "home", component: HomeComponent },
    { path: 'photographies', component: AllPhotographiesComponent },
    { path: 'photographie/news', component: NewsComponent },
    { path: 'photographie/lasts', component: LastsComponent },
    { path: 'photographie/:id', component: SinglePhotographieComponent },
    { path: 'artistes', component: ArtistesComponent },
    { path: 'artiste/:idArtiste', component: SingleArtisteComponent },
    { path: 'panier', component: PanierComponent },
    // { path: 'achat/:idPhotographie', component: AchatComponent },
    // { path: 'payement', component: PaiementComponent },
    { path: 'account', component: AccountComponent },
    { path: 'account/adresse', component: AddressComponent },
    { path: 'account/user-informations', component: UserInfoComponent },
    { path: 'account/commandes', component: OrdersComponent },
    { path: 'account/admin/comptes', component: GestionComptesComponent },
    { path: 'account/admin/commandes', component: GestionCommandesComponent },
    { path: 'create-account', component: CreateAccountComponent },
    // { path: 'user', component: UserComponent },
    { path: 'help', component: HelpComponent },
    { path: 'mentions', component: MentionLegalesComponent },
    { path: 'conditions', component: ConditionUtilisationComponent },
    { path: '', component: HomeComponent },
    { path: '**', component: NotFoundComponent }
];