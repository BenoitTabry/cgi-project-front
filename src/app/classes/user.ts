export class User {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    isBlocked: boolean;
    isAdmin: boolean;
    isVendeur: boolean;
}