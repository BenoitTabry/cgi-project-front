export class DonneePersonnelles {
    civilite: string;
    prenom: string;
    nom: string;
    telephone: string;
}
