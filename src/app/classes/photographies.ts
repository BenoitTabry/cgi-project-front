export class Photographies {
    id_Photo: number;
    prix_Base: number;
    titre_Photo: string;
    image_Url: string;
    image_Url2: string;
    nb_Tirages: number;
    nb_Vente: number;
    stock: number;
    date_Mise_en_vente: string;
    artiste: Artiste;
    commandePhoto: any[];
    tag: any[];
    orientation: Orientation;
    format: any[];
    theme: any[];
    decorations: any[];
    dispo_Galery: boolean;
    dispo_Livraison: boolean;
  }
  
  interface Orientation {
    id_Orientation: number;
    orientation: string;
  }
  
  interface Artiste {
    nom: string;
    prenom: string;
    presentation: string;
    reseauxSociaux: ReseauxSociaux;
  }
  
  interface ReseauxSociaux {
    id_Reseaux_Sociaux: number;
    facebook_Url: string;
    twitter_Url: string;
    pinteres_Url: string;
  }