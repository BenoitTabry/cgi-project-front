//modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';

//components
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { HomeComponent } from './pages/home/home.component';
import { AllPhotographiesComponent } from './pages/photographies/all-photographies/all-photographies.component';
import { NewsComponent } from './pages/photographies/news/news.component';
import { LastsComponent } from './pages/photographies/lasts/lasts.component';
import { FooterComponent } from './shared/footer/footer.component';
import { ArtistesComponent } from './pages/artistes/artistes/artistes.component';
import { SingleArtisteComponent } from './pages/artistes/single-artiste/single-artiste.component';
import { PanierComponent } from './pages/panier/panier.component';
import { MentionLegalesComponent } from './pages/footer-pages/mention-legales/mention-legales.component';
import { ConditionUtilisationComponent } from './pages/footer-pages/condition-utilisation/condition-utilisation.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { HelpComponent } from './pages/help/help.component';
import { AccountComponent } from './account/account/account.component';
import { CreateAccountComponent } from './account/create-account/create-account.component';
import { SinglePhotographieComponent } from './pages/photographies/single-photographie/single-photographie.component';
import { AddressComponent } from './account/account/address/address.component';
import { UserInfoComponent } from './account/account/user-info/user-info.component';
import { OrdersComponent } from './account/account/orders/orders.component';
import { ArianeComponent } from './shared/ariane/ariane.component';
import { GestionComptesComponent } from './account/admin/gestion-comptes/gestion-comptes.component';
import { GestionCommandesComponent } from './account/admin/gestion-commandes/gestion-commandes.component';

//others
import { appRoutes } from './interfaces/routes';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    AllPhotographiesComponent,
    NewsComponent,
    LastsComponent,
    FooterComponent,
    ArtistesComponent,
    SingleArtisteComponent,
    PanierComponent,
    MentionLegalesComponent,
    ConditionUtilisationComponent,
    HelpComponent,
    CreateAccountComponent,
    NotFoundComponent,
    AccountComponent,
    SinglePhotographieComponent,
    AddressComponent,
    UserInfoComponent,
    OrdersComponent,
    ArianeComponent,
    GestionComptesComponent,
    GestionCommandesComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    NgxPaginationModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
